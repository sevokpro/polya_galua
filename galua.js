function GF8 (val) {
	'use strict'

	this.val = val;

	GF8.prototype.polynom = 283; // equivalent 100011011

	GF8.prototype.plus = function (b) {
		this.val = this.val ^ b.val;
		return this;
	}

	GF8.prototype.multi = function (b) {
		let buf = 0;
		for ( let i = 0; i < 8; i++){
			if ( (this.val & ( 1 << i) ) !== 0 ){
				buf = buf ^ (b.val << i);
			}
		}	

		let numerator = buf.toString(2).length;
		let denumenator = this.polynom.toString(2).length;

		for ( let i = numerator - denumenator; i >= 0; i-- ){
			buf = buf ^ ( this.polynom << i );
		}

		this.val = buf;
		return this;
	}

	GF8.prototype.result = function (st) {
		let st2 = st || 2;
		// console.log(this.val.toString(st2))
		return this.val.toString(st2);
	}
}

function generator (input) {
	'use strict'

	this.data = {
		rand: [],
		input: []
	};
	for ( let key in input ){
		this.data.rand.push( new GF8( ~~( Math.random()*255 ) ) )
		this.data.input.push( new GF8(input[key]) );
	}



	generator.prototype.step = function () {
		let out = new GF8(0);
		for ( let key in this.data.input ){
			out = out.plus( this.data.rand[key].multi(this.data.input[key]) );
		}
		this.data.rand = this.data.rand.slice( 0,3 );
		this.data.rand.push( out );
		return ( out.result( 10 ) );
	}

	generator.prototype.generate = function (count) {
		let out = [];
		for (var i = 0; i < count; i++) {
			out.push( this.step() )
		};
		return out
	}
}

// var a = new GF8(5);
// var b = new GF8(7);
// a.result();
// b.result();
// a.plus(b).result();
// a.multi(b).result();

// var input = [2, 3, 5, 7];
// var gen = new generator(input);
// gen.generate(100);