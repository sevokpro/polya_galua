var canvas_container = document.getElementById('canvas');

var canvas = document.createElement('canvas');
canvas.width = 512;
canvas.height = 512;
canvas.style.backgroundColor = "gray";
canvas_container.appendChild( canvas );


function paint ( canvas ) {
	this.field = canvas.getContext("2d");
	this.field.fillStyle = "red"
	paint.prototype.putPixel = function ( x, y ) {
		this.field.fillRect( x % 512 ,y % 512 ,1 ,1 );
	}
	paint.prototype.putPixels = function (data) {
		for (var i = 0; i < data.length; i+= 2) {
			this.putPixel(data[i], data[i+1]);
		};
	}
	paint.prototype.clear = function () {
		this.field.clearRect(0, 0, 512, 512);
	}
}

var p = new paint ( canvas );

// var input = [2, 103, 57, 7];
var input = [2, 3, 5, 7];
var gen = new generator(input);
